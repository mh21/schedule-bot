# schedule-bot

Bots that run on a schedule

## Purpose

This repository contains the source code for all Python/Bash CKI bots that run
on a schedule, i.e. as a cron job.

## Available bots

### `pipeline_bot`

Interact with a user in a merge request for the pipeline definition, and
trigger pipelines for it.

## Development

### Locally building the schedule-bot image

```shell
podman run \
    -v .:/code \
    -w /code \
    -e IMAGE_NAME=schedule-bot \
    --privileged \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

[Data Warehouse]: https://gitlab.com/cki-project/datawarehouse
