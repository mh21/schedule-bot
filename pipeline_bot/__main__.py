"""Interface to python -m for the pipeline trigger."""
import json
import os

from . import main

main(json.loads(os.environ['SCHEDULER_CONFIG']))
