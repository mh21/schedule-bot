#!/usr/bin/python3
"""Test the pipeline bot module."""
import os
import re
import time
import unittest
import uuid

from cki_lib import misc
import gitlab

import pipeline_bot
from schedule_bot import utils


# pylint: disable=too-many-public-methods
class TestPipelineBot(unittest.TestCase):
    """Test the pipeline bot module."""

    def __init__(self, *args, **kwargs):
        """Create a new test case."""
        super().__init__(*args, **kwargs)
        self.merge_requests = []
        self.forked_projects = []
        self.config_overlay = {}

    @classmethod
    def setUpClass(cls):
        """Set up the Gitlab instances."""
        cls.mr_instance_url = os.environ['IT_GITLAB_MR_URL']
        cls.mr_token_member = os.environ['IT_GITLAB_MR_MEMBER_TOKEN']
        cls.mr_project_name = os.environ['IT_GITLAB_MR_PROJECT_NAME']
        cls.gl_member = gitlab.Gitlab(cls.mr_instance_url,
                                      cls.mr_token_member)
        cls.gl_member.auth()
        cls.gl_member_project = cls.gl_member.projects.get(
            cls.mr_project_name)
        cls.gl_mr_username = cls.gl_member.user.username

        cls.mr_token_contributor = os.environ['IT_GITLAB_MR_CONTRIBUTOR_TOKEN']
        cls.gl_contributor = gitlab.Gitlab(cls.mr_instance_url,
                                           cls.mr_token_contributor)
        cls.gl_contributor.auth()
        cls.gl_contributor_project = cls.gl_contributor.projects.get(
            cls.mr_project_name)

        cls.pipeline_url = os.environ['IT_GITLAB_PIPELINES_URL']
        cls.pipelines_token = os.environ['IT_GITLAB_PIPELINES_TOKEN']
        cls.cki_project_name = os.environ['IT_GITLAB_PIPELINES_PROJECT_CKI']
        cls.brew_project_name = os.environ['IT_GITLAB_PIPELINES_PROJECT_BREW']
        cls.gl_pipeline = gitlab.Gitlab(cls.pipeline_url,
                                        cls.pipelines_token)
        cls.gl_pipeline.auth()

    def tearDown(self):
        """Clean generated artifacts from the Gitlab instance."""
        utils.cleanup_gitlab(self.merge_requests + self.forked_projects)

    def check_welcome_message(self, merge_request):
        """Check that exactly one welcome message gets generated."""
        self.run_trigger(merge_request)
        notes = [n for n in merge_request.notes.list()
                 if n.author['username'] == self.gl_mr_username]
        self.assertEqual(len(notes), 1)
        self.assertIn('WELCOME_MESSAGE', notes[0].body)

    def check_reply(self, reply, forbidden=False):
        """Check whether the user has permission to start testing."""
        self.assertEqual('permission' in reply['body'], forbidden)

    def check_pipeline_request(self, what, pipelines):
        """Request pipelines and verify the correct branches are used."""
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)
        reply = self.request_testing(merge_request, what)
        try:
            self.check_pipeline_branches(reply, pipelines)
        finally:
            self.cancel_pipelines(reply)

    def check_pipeline_branches(self, reply, expected_pipelines):
        """Verify the include files, trigger variables and prepared repos."""
        pipelines = set()
        for pipeline in pipeline_bot.parse_status_note(reply['body']):
            project = self.gl_pipeline.projects.get(pipeline['project'])
            g_pipeline = project.pipelines.get(pipeline['id'])
            original_branch = self.parent_branch(project, g_pipeline.sha)
            pipelines.add((pipeline['project'], original_branch))
        self.assertEqual(pipelines, expected_pipelines)

    # pylint: disable=too-many-locals, too-many-arguments
    def _check_pipeline(self, pipeline, source_project, merge_request,
                        override_type_other, expected_variables):
        project = self.gl_pipeline.projects.get(pipeline['project'])
        g_pipeline = project.pipelines.get(pipeline['id'])

        # verify included files
        urls = re.findall('http.*', project.files.get(
            '.gitlab-ci.yml', ref=g_pipeline.sha).decode().decode('utf-8'))
        if override_type_other:
            include_project = self.gl_member_project
            include_branch = 'main'
        else:
            include_project = source_project
            include_branch = merge_request.source_branch
        include_part = '/'.join((include_project.path_with_namespace,
                                 'raw', include_branch))
        self.assertTrue(all(include_part in u for u in urls))

        # verify trigger variables
        variables = {v.key: v.value
                     for v in g_pipeline.variables.list(as_list=False)}
        for key, value in expected_variables.items():
            self.assertEqual(variables.get(key), value)
        if override_type_other:
            self.assertNotIn(
                'pipeline_definition_repository_override', variables)
            self.assertNotIn(
                'pipeline_definition_branch_override', variables)
        else:
            self.assertEqual(
                variables.get('pipeline_definition_repository_override'),
                source_project.web_url)
            self.assertEqual(
                variables.get('pipeline_definition_branch_override'),
                merge_request.source_branch)

            # verify output
            prepare_job_id = next(j for j in g_pipeline.jobs.list
                                  (as_list=False) if j.stage == 'prepare').id
            prepared = False
            while not prepared:
                prepare_job = project.jobs.get(prepare_job_id)
                prepared = (prepare_job.status not in
                            ('created', 'pending', 'running'))
            output = prepare_job.trace().decode('utf-8')
            self.assertIn(source_project.path_with_namespace, output)
            self.assertIn(merge_request.source_branch, output)

    def check_pipeline_projects(self, reply, source_project, merge_request,
                                override_type_other=False,
                                expected_variables=None):
        """Verify the include files, trigger variables and prepared repos."""
        for pipeline in pipeline_bot.parse_status_note(reply['body']):
            self._check_pipeline(pipeline, source_project, merge_request,
                                 override_type_other, expected_variables or {})

    def check_beaker(self, what, expected_value):
        """Check that skip_beaker is set by default."""
        self.check_variables(what, {'skip_beaker': expected_value})

    def check_variables(self, what, expected_variables):
        """Check that variables are overridden correctly."""
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)
        reply = self.request_testing(merge_request, what)
        try:
            pipeline = pipeline_bot.parse_status_note(reply['body'])[0]
            project = self.gl_pipeline.projects.get(pipeline['project'])
            g_pipeline = project.pipelines.get(pipeline['id'])
            variables = {v.key: v.value
                         for v in g_pipeline.variables.list(as_list=False)}
            for key, value in expected_variables.items():
                self.assertEqual(variables.get(key), value)
        finally:
            self.cancel_pipelines(reply)

    def cancel_pipelines(self, reply, delete=True):
        """Cancel and delete all pipelines referenced by a status note."""
        for pipeline in pipeline_bot.parse_status_note(reply['body']):
            with misc.only_log_exceptions():
                project = self.gl_pipeline.projects.get(pipeline['project'])
                g_pipeline = project.pipelines.get(pipeline['id'])
                if delete:
                    g_pipeline.delete()
                else:
                    g_pipeline.cancel()

    @staticmethod
    def parent_branch(project, sha, depth=5):
        """Return the branch of the first (parent) commit with a ref."""
        for _ in range(depth):
            commit = project.commits.get(sha)
            branch = next((r['name'] for r in commit.refs()
                           if r['type'] == 'branch'), None)
            if branch:
                return branch
            if not commit.parent_ids:
                break
            sha = commit.parent_ids[0]
        return None

    def request_testing(self, merge_request, what=''):
        """Request testing and return the reply note."""
        discussion = merge_request.discussions.create({
            'body': f'@{self.gl_mr_username} please test {what}'
        })
        self.run_trigger(merge_request)
        discussion = merge_request.discussions.get(discussion.id)
        replies = [n for n in discussion.attributes['notes'][1:]
                   if n['author']['username'] == self.gl_mr_username]
        self.assertEqual(len(replies), 1)
        return replies[0]

    def merge_config(self, first, second):
        """Merge the second dict into the first, and allow to delete keys."""
        for key, value in second.items():
            if isinstance(first.get(key), dict) and isinstance(value, dict):
                first[key] = self.merge_config(first[key], value)
            elif value is None:
                del first[key]
            else:
                first[key] = value
        return first

    def run_trigger(self, merge_request):
        """Run the GitLab trigger."""
        os.environ['IT_GITLAB_MERGE_REQUEST'] = str(merge_request.iid)
        module_config = {
            'projects': {
                'pipeline-definition': {
                    'gitlab_url': self.mr_instance_url,
                    'project': self.mr_project_name,
                    'private_token': 'IT_GITLAB_MR_MEMBER_TOKEN',
                    'default_pipelines': ['cki'],
                    'welcome_message': 'WELCOME_MESSAGE',
                    'override': {
                        'type': 'pipeline-definition',
                        'ref': 'branch',
                    }
                },
            },
            'pipelines': {
                'cki': {
                    'name': 'CKI pipeline',
                    'gitlab_url': self.pipeline_url,
                    'project': self.cki_project_name,
                    'private_token': 'IT_GITLAB_PIPELINES_TOKEN',
                    'default_branches': ['rhel8', 'rhel7', 'upstream-stable'],
                },
                'brew': {
                    'name': 'Brew pipeline',
                    'gitlab_url': self.pipeline_url,
                    'project': self.brew_project_name,
                    'private_token': 'IT_GITLAB_PIPELINES_TOKEN',
                    'default_branches': ['rhel8', 'rhel7'],
                },
            }
        }
        self.merge_config(module_config, self.config_overlay)
        pipeline_bot.main(module_config)

    def fork_project(self, instance: gitlab.Gitlab, project):
        """For a GitLab project for the user of the Gitlab instance."""
        name = f'{project.path}-{uuid.uuid4()}'
        fork = project.forks.create({
            'name': name,
            'path': name,
        })
        time.sleep(10)  # give GitLab some time for the forking
        forked_project = instance.projects.get(fork.id)
        print(forked_project.web_url)
        self.forked_projects.append(forked_project)
        return forked_project

    # pylint: disable=no-self-use
    def create_branch(self, project, source_ref='main'):
        """Create a modified pipeline branch."""
        branch = project.branches.create({
            'branch': f'inttest-{uuid.uuid4()}',
            'ref': source_ref,
        })
        pipelines_content = project.files.get(
            'cki_pipeline.yml',
            branch.commit['id']).decode().decode('utf-8')
        pipelines_content = '# Integration test\n' + pipelines_content
        project.commits.create({
            'branch': branch.name,
            'commit_message': 'Integration test',
            'actions': [{
                'action': 'update',
                'file_path': 'cki_pipeline.yml',
                'content': pipelines_content,
            }]
        })
        return branch

    def create_mr(self, project, branch, target_project, target_ref='main'):
        """Create a merge request in the target project."""
        merge_request = project.mergerequests.create({
            'source_branch': branch.name,
            'target_project_id': target_project.id,
            'target_branch': target_ref,
            'title': 'integration test',
        })
        print(merge_request.web_url)
        target_merge_request = self.gl_member_project.mergerequests.get(
            merge_request.iid)
        self.merge_requests.append(target_merge_request)
        return merge_request

    def test_member(self):
        """Test the interaction with a member."""
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)

        # Check the generation of a welcome message on bot run
        self.check_welcome_message(merge_request)

        # Check that no welcome message are generated on subsequent runs
        self.check_welcome_message(merge_request)

        # Request some pipelines
        reply = self.request_testing(merge_request, '[cki/rhel8]')

        # Check that the user has permission to start testing
        self.check_reply(reply)

        # Check that the pipelines use the correct repos
        self.check_pipeline_projects(reply, forked_project, merge_request)

        # Kill the pipelines
        self.cancel_pipelines(reply)

    def test_contributor(self):
        """Test the limited interaction with an external contributor."""
        forked_project = self.fork_project(
            self.gl_contributor, self.gl_contributor_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_contributor_project)

        # Check the generation of a welcome message on bot run
        self.check_welcome_message(merge_request)

        # Check that no welcome message are generated on subsequent runs
        self.check_welcome_message(merge_request)

        # Request some pipelines
        reply = self.request_testing(merge_request, '[cki/rhel8]')

        # Check that the user has no permission to start testing
        self.check_reply(reply, forbidden=True)

        # Request some pipelines as a member
        elevated_merge_request = self.gl_member_project.mergerequests.get(
            merge_request.iid)
        reply = self.request_testing(elevated_merge_request, '[cki/rhel8]')

        # Check that the merge request can be submitted by a member
        self.check_reply(reply)

        # Check that the pipelines use the correct repos
        self.check_pipeline_projects(reply, forked_project, merge_request)

        # Kill the pipelines
        self.cancel_pipelines(reply)

    def test_wrong_message(self):
        """Test unknown tag handling."""
        forked_project = self.fork_project(
            self.gl_contributor, self.gl_contributor_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_contributor_project)

        # Wrong tag with low privileges.
        reply = self.request_testing(merge_request, '[wrong/tag]')
        self.check_reply(reply, forbidden=True)

        elevated_merge_request = self.gl_member_project.mergerequests.get(
            merge_request.iid)

        # Wrong tag with enough privileges.
        reply = self.request_testing(elevated_merge_request, '[wrong/tag]')
        self.assertTrue('try again' in reply['body'])

        reply = self.request_testing(elevated_merge_request, '[wrong tag]')
        self.assertTrue('try again' in reply['body'])

        reply = self.request_testing(elevated_merge_request, '[wrong]')
        self.assertTrue('try again' in reply['body'])

    def test_group_default(self):
        """Request the default group."""
        self.check_pipeline_request('', {
            (self.cki_project_name, 'rhel7'),
            (self.cki_project_name, 'rhel8'),
            (self.cki_project_name, 'upstream-stable'),
        })

    def test_group_another(self):
        """Request a specific group."""
        self.check_pipeline_request('[brew]', {
            (self.brew_project_name, 'rhel7'),
            (self.brew_project_name, 'rhel8'),
        })

    def test_group_backslashes(self):
        """Check that backslashes in groups are ignored."""
        self.check_pipeline_request(r'\[brew\]', {
            (self.brew_project_name, 'rhel7'),
            (self.brew_project_name, 'rhel8'),
        })

    def test_group_no_brackets(self):
        """Check that both brackets are required."""
        for what in ('brew', '[brew', 'brew]'):
            self.check_pipeline_request(what, {
                (self.cki_project_name, 'rhel7'),
                (self.cki_project_name, 'rhel8'),
                (self.cki_project_name, 'upstream-stable'),
            })

    def test_group_multiple(self):
        """Request multiple groups."""
        self.check_pipeline_request('[cki][brew]', {
            (self.cki_project_name, 'rhel7'),
            (self.cki_project_name, 'rhel8'),
            (self.cki_project_name, 'upstream-stable'),
            (self.brew_project_name, 'rhel7'),
            (self.brew_project_name, 'rhel8'),
        })

    def test_group_all(self):
        """Request all groups."""
        self.check_pipeline_request('[all]', {
            (self.cki_project_name, 'rhel7'),
            (self.cki_project_name, 'rhel8'),
            (self.cki_project_name, 'upstream-stable'),
            (self.brew_project_name, 'rhel7'),
            (self.brew_project_name, 'rhel8'),
        })

    def test_group_all_visible(self):
        """Request all groups, but limited to the visible ones."""
        self.config_overlay = {
            'projects': {
                'pipeline-definition': {
                    'visible_pipelines': ['cki'],
                }}}
        self.check_pipeline_request('[all]', {
            (self.cki_project_name, 'rhel7'),
            (self.cki_project_name, 'rhel8'),
            (self.cki_project_name, 'upstream-stable'),
        })

    def test_branch_single(self):
        """Request a single branch."""
        self.check_pipeline_request('[cki/rhel8]', {
            (self.cki_project_name, 'rhel8'),
        })

    def test_branch_id(self):
        """Request a single pipeline ID."""
        self.check_pipeline_request('[cki/627727]', {
            (self.cki_project_name, 'rhel8'),
        })

    def test_branch_mixed_branch_id(self):
        """Request pipeline both by branch and ID."""
        self.check_pipeline_request('[cki/rhel7][cki/627727]', {
            (self.cki_project_name, 'rhel7'),
            (self.cki_project_name, 'rhel8'),
        })

    def test_branch_no_default(self):
        """Request a non-default pipeline."""
        self.check_pipeline_request('[cki/kernel-rt-rhel8]', {
            (self.cki_project_name, 'kernel-rt-rhel8'),
        })

    def test_branch_single_upstream(self):
        """Request a single branch with a dash."""
        self.check_pipeline_request('[cki/upstream-stable]', {
            (self.cki_project_name, 'upstream-stable'),
        })

    def test_branch_multiple(self):
        """Request branches from different groups."""
        self.check_pipeline_request('[cki/rhel8][brew/rhel8]', {
            (self.cki_project_name, 'rhel8'),
            (self.brew_project_name, 'rhel8'),
        })

    def test_branch_only_backslashes(self):
        """Check that backslashes in "only" are ignored."""
        self.check_pipeline_request(r'\[cki/rhel8\]', {
            (self.cki_project_name, 'rhel8'),
        })

    def test_email_notification(self):
        """Check that the two plain-text emails are generated."""
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)
        reply = self.request_testing(merge_request, '[cki/rhel8]')
        try:
            self.cancel_pipelines(reply, delete=False)
            self.run_trigger(merge_request)
        finally:
            self.cancel_pipelines(reply)

    def test_skip_beaker_true(self):
        """Check that skip_beaker is set by default."""
        self.check_beaker('[cki/rhel8]', 'true')

    def test_skip_beaker_false(self):
        """Check that skip_beaker can be set to false if requested."""
        self.check_beaker('[cki/rhel8][skip_beaker=false]', 'false')

    def test_skip_beaker_backslashes(self):
        """Check that backslashes while setting skip_beaker are ignored."""
        self.check_beaker(r'\[cki/rhel8\]\[skip_beaker=false\]', 'false')

    def test_variables_skip_beaker(self):
        """Check that variables can be overridden."""
        self.check_variables('[cki/rhel8][skip_beaker=false][foo=baz]'
                             '[architectures=x86_64 s390x]', {
                                 'skip_beaker': 'false',
                                 'foo': 'baz',
                                 'architectures': 'x86_64 s390x',
                             })

    def test_override_none(self):
        """Test the None override mode."""
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)

        self.config_overlay = {'projects': {'pipeline-definition': {
            'override': None,
        }}}
        reply = self.request_testing(merge_request, '[cki/rhel8]')

        try:
            self.check_pipeline_projects(reply, forked_project, merge_request,
                                         override_type_other=True)
        finally:
            self.cancel_pipelines(reply)

    def _test_override_pip_url(self, ref, url):
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)
        if ref in ('merge', 'head'):
            ref_name = f'refs/merge-requests/{merge_request.iid}/{ref}'
            ref_id = self.gl_member_project.commits.get(ref_name).id
        else:
            ref_name = None
            ref_id = None

        self.config_overlay = {'projects': {'pipeline-definition': {
            'override': {'type': 'pip_url', 'name': 'variable_name', 'ref': ref},
        }}}
        reply = self.request_testing(merge_request, '[cki/rhel8]')

        try:
            self.check_pipeline_projects(
                reply, forked_project, merge_request, override_type_other=True,
                expected_variables={'variable_name': url.format_map({
                    'gl_member': self.gl_member,
                    'forked_project': forked_project,
                    'project': self.gl_member_project,
                    'merge_request': merge_request,
                    'ref_name': ref_name,
                    'ref_id': ref_id,
                })})
        finally:
            self.cancel_pipelines(reply)

    def test_override_pip_url_branch(self):
        """Test the pip_url override mode."""
        self._test_override_pip_url(
            'branch',
            'git+{forked_project.web_url}.git/@{merge_request.source_branch}')

    def test_override_pip_url_merge(self):
        """Test the pip_url override mode with merge refs."""
        self._test_override_pip_url(
            'merge',
            'git+{project.web_url}.git/@{ref_name}')

    def test_override_pip_url_head(self):
        """Test the pip_url override mode with head refs."""
        self._test_override_pip_url(
            'head',
            'git+{project.web_url}.git/@{ref_name}')

    def _test_override_archive_url(self, ref, url):
        forked_project = self.fork_project(
            self.gl_member, self.gl_member_project)
        branch = self.create_branch(forked_project)
        merge_request = self.create_mr(forked_project, branch,
                                       self.gl_member_project)
        if ref in ('merge', 'head'):
            ref_id = self.gl_member_project.commits.get(
                f'refs/merge-requests/{merge_request.iid}/{ref}').id
        else:
            ref_id = None

        self.config_overlay = {'projects': {'pipeline-definition': {
            'override': {'type': 'archive_url', 'name': 'variable_name', 'ref': ref},
        }}}
        reply = self.request_testing(merge_request, '[cki/rhel8]')

        try:
            self.check_pipeline_projects(
                reply, forked_project, merge_request, override_type_other=True,
                expected_variables={'variable_name': url.format_map({
                    'gl_member': self.gl_member,
                    'forked_project': forked_project,
                    'project': self.gl_member_project,
                    'merge_request': merge_request,
                    'ref_id': ref_id,
                })})
        finally:
            self.cancel_pipelines(reply)

    def test_override_archive_url_branch(self):
        """Test the archive_url override mode."""
        self._test_override_archive_url(
            'branch',
            '{gl_member.api_url}/projects/{forked_project.id}/repository/archive.zip'
            '?sha={merge_request.sha}')

    def test_override_archive_url_merge(self):
        """Test the archive_url override mode with merge refs."""
        self._test_override_archive_url(
            'merge',
            '{gl_member.api_url}/projects/{project.id}/repository/archive.zip'
            '?sha={ref_id}')

    def test_override_archive_url_head(self):
        """Test the archive_url override mode with head refs."""
        self._test_override_archive_url(
            'head',
            '{gl_member.api_url}/projects/{project.id}/repository/archive.zip'
            '?sha={ref_id}')

    def test_variable_filter(self):
        """Check that successful pipelines can be filtered by variables."""
        self.config_overlay = {'pipelines': {'cki': {'variable_filter': {
            'cki_pipeline_type': 'baseline',
        }}}}
        self.check_variables('[cki/rhel8]', {
            'cki_pipeline_type': 'retrigger-baseline'
        })

    def test_tests_only(self):
        """Check that a project can request to only run tests."""
        self.config_overlay = {'projects': {'pipeline-definition': {
            'tests_only': True,
        }}}
        self.check_variables('[cki/rhel8]', {
            'skip_build': 'true',
            'skip_merge': 'true',
        })
