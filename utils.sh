#!/bin/bash

function echo_green {
    echo -e "\e[1;32m$*\e[0m"
}

function echo_red {
    echo -e "\e[1;31m$*\e[0m"
}

function echo_yellow {
    echo -e "\e[1;33m$*\e[0m"
}

function parse_bucket_spec {
    IFS='|' read -r AWS_ENDPOINT AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_BUCKET AWS_BUCKET_PATH <<< "${!BUCKET_CONFIG_NAME}"
    export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
}

